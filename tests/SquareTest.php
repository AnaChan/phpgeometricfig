<?php

// /////////////////////////////////////////////////////////////////////////////
// TESTING AREA
// THIS IS AN AREA WHERE YOU CAN TEST YOUR WORK AND WRITE YOUR OWN UNIT TESTS
// /////////////////////////////////////////////////////////////////////////////

use App\Exceptions\WrongParamCountException;
use App\Square;
use App\GeometricShape;
use App\PolygonInterface;
use App\ShapeFactory;
use App\ShapeInterface;
use PHPUnit\Framework\TestCase;

class SquareTest extends TestCase
{
    protected function getValidSquare()
    {
        return new Square(3);
    }

    public function testShouldCreateSquareObject()
    {
        $this->assertInstanceOf(Square::class, ShapeFactory::createShape("Square", [3]));
        $this->assertInstanceOf(Square::class, ShapeFactory::createShape("Square", [3.65]));
    }

    public function testShouldThrowWrongParamCountExceptionWithWrongNumberOfParameters()
    {
        $this->expectException(WrongParamCountException::class);
        ShapeFactory::createShape("Square", []);
        ShapeFactory::createShape("Square", [1,1]);
        ShapeFactory::createShape("Square", [1, 1, 1]);
    }

    public function testSquareeObjectHasTheCorrectName()
    {
        $square = $this->getValidSquare();
        $this->assertEquals("Square", $square->getName());
    }

    public function testImplementsTheShapeInterface()
    {
        $square = $this->getValidSquare();
        $this->assertInstanceOf(ShapeInterface::class, $square);
    }

    public function testImplementPolygonInterface()
    {
        $square = $this->getValidSquare();
        $this->assertInstanceOf(PolygonInterface::class, $square);
    }

    public function testExtendsTheGeometricShape()
    {
        $square = $this->getValidSquare();
        $this->assertInstanceOf(GeometricShape::class, $square);
    }

    public function testReturnsTheCorrectPerimeter()
    {
        $square = $this->getValidSquare();
        $this->assertEquals(12, $square->getPerimeter());
    }

    public function testReturnsTheCorrectArea()
    {
        $square = $this->getValidSquare();
        $this->assertEquals(9, $square->getArea());
    }

    public function testReturnsTheCorrectAngles()
    {
        $square = $this->getValidSquare();
        $this->assertEquals(4, $square->getAngles());
    }
}